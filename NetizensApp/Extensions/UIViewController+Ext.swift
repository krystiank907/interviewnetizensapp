//
//  UIViewController+Ext.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit

extension UIViewController {
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.topAnchor
        } else {
            return topLayoutGuide.bottomAnchor
        }
    }
    
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return view.safeAreaLayoutGuide.bottomAnchor
        } else {
            return bottomLayoutGuide.topAnchor
        }
    }
}
