// Generated using Sourcery 0.16.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import UIKit



//MARK: - Create Views for NAPhotoDetailCollectionCell
extension NAPhotoDetailCollectionCell {

    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["thumbImageView"] = thumbImageView
        dictionary["titleLabel"] = titleLabel
        dictionary["loadingIndicator"] = loadingIndicator
        return dictionary
    }

    @objc dynamic func createViews() {

        let thumbImageView = UIImageView()
        contentView.addSubview(thumbImageView)
        self.thumbImageView = thumbImageView
        thumbImageView.translatesAutoresizingMaskIntoConstraints = false

        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .appFont()
        contentView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        let loadingIndicator = UIActivityIndicatorView()
        contentView.addSubview(loadingIndicator)
        self.loadingIndicator = loadingIndicator
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false

    }
}




//MARK: - Create Views for NAPhotoTableViewCell
extension NAPhotoTableViewCell {

    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["containerView"] = containerView
        dictionary["thumbImageView"] = thumbImageView
        dictionary["titleLabel"] = titleLabel
        dictionary["loadingIndicator"] = loadingIndicator
        return dictionary
    }

    @objc dynamic func createViews() {

        let containerView = UIView()
        contentView.addSubview(containerView)
        self.containerView = containerView
        containerView.translatesAutoresizingMaskIntoConstraints = false

        let thumbImageView = UIImageView()
        containerView.addSubview(thumbImageView)
        self.thumbImageView = thumbImageView
        thumbImageView.translatesAutoresizingMaskIntoConstraints = false

        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = .appFont()
        containerView.addSubview(titleLabel)
        self.titleLabel = titleLabel
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        let loadingIndicator = UIActivityIndicatorView()
        containerView.addSubview(loadingIndicator)
        self.loadingIndicator = loadingIndicator
        loadingIndicator.translatesAutoresizingMaskIntoConstraints = false

    }
}




//MARK: - Create Views for NAPhotoDetailViewController
extension NAPhotoDetailViewController {

    @objc dynamic var viewsDictionary: [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary["dissmisButton"] = dissmisButton
        return dictionary
    }

    @objc dynamic func createViews() {

        let dissmisButton = UIButton(type: .system)

        view.addSubview(dissmisButton)
        self.dissmisButton = dissmisButton
        dissmisButton.translatesAutoresizingMaskIntoConstraints = false

    }
}

