//
//  NASyncManager.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//
import Foundation
import Alamofire
import RealmSwift

public typealias ConnectionCompletionHandler = (_ error: Error?) -> ()


open class NASyncManager{
    
    public static let shered = NASyncManager()
    open var sessionManager:Session = Session(configuration: URLSessionConfiguration.default)
    
    
    func downloadData(dataType: NADataCollection, completionHandler:ConnectionCompletionHandler?){
        sessionManager.request(dataType.urlString).responseJSON { response in
            
            switch response.result{
            case .success(_):
                if let value = response.data, let json = try? JSONSerialization.jsonObject(with: value, options: []) as? [[String:Any]] {
                    
                    let objects = dataType.objects(from: json)
                    let realm = try! Realm()
                    realm.beginWrite()
                    realm.add(objects, update: .modified)
                    try! realm.commitWrite()
                    completionHandler?(nil)
                }else{
                    completionHandler?(nil)
                }
            case .failure(let error):
                completionHandler?(error)
            }

        }
    }
}

enum NADataCollection: CaseIterable {
    case photos
    
    
    var urlString: String{
        switch self{
        case .photos:
            return "https://jsonplaceholder.typicode.com/photos"
        }
    }
    
    func objects(from json: Any) -> [Object] {
        switch self {
        case .photos:
            return (try? JSONDecoder().decode([RLMPhotoList].self, from: json)) ?? []
        }
    }
    
}

extension JSONDecoder {
    open func decode<T>(_ type: T.Type, from json: Any) throws -> T where T : Decodable {
        let data = try JSONSerialization.data(withJSONObject: json, options: [])
        return try decode(type, from: data)
    }
}
