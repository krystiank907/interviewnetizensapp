//
//  NAPhotoDetailViewController.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart
import RxSwift
import RxCocoa

class NAPhotoDetailViewController: DSViewController, DSCollectionControllerWithViewModel {
    
    
    weak var dissmisButton: UIButton!

    //sourcery:begin: notCreate
    var scrollDirection: UICollectionView.ScrollDirection{
        return .horizontal
    }
    var viewModel:VMPhotoDetail
    //sourcery:end
    
    init(viewModel:VMPhotoDetail){
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.collectionViewSource = DSCollectionDataSource(viewModel: viewModel, viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        createConstraints()
        observeCollectionViewModel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.scrollToItem(at: viewModel.selectedIndexPath, at: .centeredHorizontally, animated: false)
    }
    
    func setupCell(_ collectionView: UICollectionView, cell: UICollectionViewCell, at indexPath: IndexPath) {
        guard let photoCell = cell as? NAPhotoDetailCollectionCell,
            let object = viewModel.collectionObjectForIndexPath(indexPath)
            else {return }
        photoCell.setup(file: object)
        
    }

}
extension NAPhotoDetailViewController: ViewControllerAutoCreateViews{
    
    func setupViews() {
        setupCollectionView()
        view.backgroundColor = UIColor(red: 0.301, green: 0.301, blue: 0.301, alpha: 1)
        collectionView.isPagingEnabled = true
        dissmisButton.setImage(UIImage(named: "closeIcon")?.withRenderingMode(.alwaysOriginal), for: .normal)
        dissmisButton.rx.tap.subscribe(onNext: {[weak self] _ in
            self?.dismiss(animated: true, completion: nil)
        }).disposed(by: disposeBag)
    }
    
    func createConstraints() {
        var viewDict = self.viewsDictionary
        viewDict["collectionView"] = collectionView
        
        var constraints: [NSLayoutConstraint] = [
        
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[collectionView]|", options: [], metrics: nil, views: viewDict),
            
            NSLayoutConstraint.constraints(withVisualFormat: "V:[dissmisButton(30)][collectionView]|", options: [], metrics: nil, views: viewDict),
            
            NSLayoutConstraint.constraints(withVisualFormat: "H:[dissmisButton(30)]-|", options: [], metrics: nil, views: viewDict),

        ].flatMap({$0})
        
        constraints.append(contentsOf: [
            dissmisButton.topAnchor.constraint(equalTo: safeTopAnchor, constant: 13),
            ])
        
        NSLayoutConstraint.activate(constraints)
    }
    
}

extension NAPhotoDetailViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        let height = collectionView.frame.height
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
