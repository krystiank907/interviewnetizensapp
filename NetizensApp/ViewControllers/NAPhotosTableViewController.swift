//
//  NAPhotosTableViewController.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import DevStart
import iProgressHUD

class NAPhotosTableViewController: DSViewController, DSTableViewControllerWithViewModel {

    var viewModel = VMPhotosTable()
    
    init(){
        super.init(nibName: nil, bundle: nil)
        self.tableViewDataSource = DSTableViewDataSource(viewModel: viewModel, viewController: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        observeTableViewModel()
        
        view.showProgress()
        NASyncManager.shered.downloadData(dataType: .photos) { [weak self] (data) in
            DispatchQueue.main.async {
                self?.view.dismissProgress()
            }
        }
        
    }
    
    func setupCell(_ tableView: UITableView, cell: UITableViewCell, at indexPath: IndexPath) {
        guard let photoCell = cell as? NAPhotoTableViewCell, let object = viewModel.tableObjectForIndexPath(indexPath) else {return }
        photoCell.setup(file: object)
    }

}
extension NAPhotosTableViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = NAPhotoDetailViewController(viewModel: VMPhotoDetail(selectedIndexPath: indexPath))
        present(vc, animated: true, completion: nil)
    }
    
}
extension NAPhotosTableViewController{
    
    func setupViews() {
        setupTableView()
        let iprogress: iProgressHUD = iProgressHUD()
        iprogress.indicatorStyle = .ballClipRotatePulse
        iprogress.attachProgress(toView: view)
        
        navigationItem.title = "Photos List"
        tableView.backgroundColor = UIColor(red: 0.301, green: 0.301, blue: 0.301, alpha: 1)
    }
    
}

