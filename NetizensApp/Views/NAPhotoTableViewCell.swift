//
//  NAPhotoTableViewCell.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import AlamofireImage

class NAPhotoTableViewCell: UITableViewCell, CellAutoCreateViews {
    
    weak var containerView: UIView!
    
    //sourcery:begin: superView = containerView
    weak var thumbImageView: UIImageView!
    weak var titleLabel: UILabel!
    weak var loadingIndicator: UIActivityIndicatorView!
    //sourcery:end
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        createViews()
        setupViews()
        createConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        thumbImageView.layer.cornerRadius = thumbImageView.bounds.height / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbImageView.image = nil
    }
}

extension NAPhotoTableViewCell:NASetupImageCell {

}

extension NAPhotoTableViewCell {
    
    func setupViews() {
        thumbImageView.backgroundColor = .lightGray
        thumbImageView.clipsToBounds = true
        loadingIndicator.hidesWhenStopped = true
        containerView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        containerView.clipsToBounds = false
        containerView.layer.cornerRadius = 8
        titleLabel.textColor = UIColor.white
    }
    
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        
        var constraints: [NSLayoutConstraint] = [
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[thumbImageView(50)]-19-[titleLabel]-26-|", options: [], metrics: nil, views: viewsDict),
            
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=8-[thumbImageView(50)]->=8@750-|", options: [], metrics: nil, views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "V:|->=8-[titleLabel]->=8@750-|", options: [], metrics: nil, views: viewsDict),
            loadingIndicator.constraints(centerTo: thumbImageView),
            containerView.constraintsEqualToSupperView(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)),
            ].flatMap{$0}
        
        constraints.append(contentsOf: [
            thumbImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
        ])
        
        NSLayoutConstraint.activate(constraints)
    }
}
