//
//  NAPhotoDetailCollectionCell.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import AlamofireImage

class NAPhotoDetailCollectionCell: UICollectionViewCell, CellAutoCreateViews {
    
    weak var thumbImageView: UIImageView!
    weak var titleLabel: UILabel!
    weak var loadingIndicator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        createViews()
        setupViews()
        createConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        thumbImageView.image = nil
    }
    
}

extension NAPhotoDetailCollectionCell:NASetupImageCell {

}

extension NAPhotoDetailCollectionCell {
    
    func setupViews() {
        thumbImageView.contentMode = .scaleAspectFit
        thumbImageView.clipsToBounds = true
        loadingIndicator.hidesWhenStopped = true
        titleLabel.textColor = UIColor.white
    }
    
    func createConstraints() {
        let viewsDict = self.viewsDictionary
        
        let constraints: [NSLayoutConstraint] = [
            thumbImageView.constraintsEqualToSupperView(),
            NSLayoutConstraint.constraints(withVisualFormat: "V:[titleLabel]-|", options: [], metrics: nil, views: viewsDict),
            NSLayoutConstraint.constraints(withVisualFormat: "H:|-[titleLabel]-|", options: [], metrics: nil, views: viewsDict),
            ].flatMap{$0}
        
        NSLayoutConstraint.activate(constraints)
    }
}
