//
//  NASetupImageCell.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import AlamofireImage
import UIKit

protocol NASetupImageCell {
    var thumbImageView: UIImageView! { get }
    var titleLabel: UILabel! { get }
    var loadingIndicator: UIActivityIndicatorView! {get }
    func setup(file: RLMPhotoList)
    func setup(imageURL:String)
}

extension NASetupImageCell {
    
    func setup(file: RLMPhotoList) {
        titleLabel.text = file.title
        setup(imageURL: file.thumbnailUrl)
    }
    
    func setup(imageURL:String) {
        guard let url = URL(string: imageURL) else { return }
        loadingIndicator.startAnimating()
        thumbImageView.af_setImage(withURL: url, completion: { (_) in
            self.loadingIndicator.stopAnimating()
        })
    }
}

