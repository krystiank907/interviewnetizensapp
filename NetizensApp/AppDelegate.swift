//
//  AppDelegate.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import UIKit
import RealmSwift
import AlamofireImage
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        #if DEBUG
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
        #endif
        createRootViewController()
        appearanceConfiguration()
        return true
    }


    
    func createRootViewController(){
        if window == nil {
            window = UIWindow()
        }
        
        window?.makeKeyAndVisible()
        let navigationViewController = UINavigationController(rootViewController: NAPhotosTableViewController())
        window?.rootViewController = navigationViewController
    }

    func appearanceConfiguration(){
        UITableViewCell.appearance().selectionStyle = .none
        
        let navigationBar = UINavigationBar.appearance()
        
        navigationBar.barStyle = .default
        navigationBar.barTintColor = UIColor(red:0.94, green:0.10, blue:0.16, alpha:1.0)
        navigationBar.backgroundColor = UIColor(red:0.94, green:0.10, blue:0.16, alpha:1.0)
        navigationBar.tintColor = .white
        navigationBar.titleTextAttributes = [
            .font: UIFont.appFont(ofSize: 16,weight: .bold),
            .foregroundColor: UIColor.white
        ]
        navigationBar.largeTitleTextAttributes = [
            .font: UIFont.appFont(ofSize: 30, weight: .bold),
            .foregroundColor: UIColor.white
        ]
    }

}


extension Realm {
    static func config() {
        let configuration = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { (migration, oldSchemaVersion) in
                if (oldSchemaVersion < 1) {
                    
                }
        })
        
        #if DEBUG
        print(configuration.fileURL!)
        #endif
        
        Realm.Configuration.defaultConfiguration = configuration
    }
}
