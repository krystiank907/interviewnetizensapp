//
//  VMPhotosTable.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift
import DevStart

class VMPhotosTable: DSViewModelRealmTableDataSource{
    
    var tableRealmDataArray: AnyRealmCollection<RLMPhotoList>!
    
    init(){
        let realm = try! Realm()
        let objectsList = realm.objects(RLMPhotoList.self)
        tableRealmDataArray = AnyRealmCollection(objectsList)
        observeTableChanges()
    }
    
    var tableItemIdentifiers: [String : AnyClass]{
        return [NAPhotoTableViewCell.className : NAPhotoTableViewCell.self]
    }
    
    
}
