//
//  VMPhotoDetail.swift
//  NetizensApp
//
//  Created by Krystian Kulawiak on 18/11/2019.
//  Copyright © 2019 Krystian Kulawiak. All rights reserved.
//

import Foundation
import RealmSwift
import DevStart

class VMPhotoDetail: DSViewModelRealmCollectionDataSource{
    
    var collectionRealmDataArray: AnyRealmCollection<RLMPhotoList>!
    
    let selectedIndexPath: IndexPath
    
    init(selectedIndexPath: IndexPath){
        self.selectedIndexPath = selectedIndexPath
        let realm = try! Realm()
        let objectsList = realm.objects(RLMPhotoList.self)
        collectionRealmDataArray = AnyRealmCollection(objectsList)
        observeCollectionChanges()
    }
    
    var collectionItemIdentifiers: [String : AnyClass]{
        return [NAPhotoDetailCollectionCell.className : NAPhotoDetailCollectionCell.self]
    }
    
    
}


