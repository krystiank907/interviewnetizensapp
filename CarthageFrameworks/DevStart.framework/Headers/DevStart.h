//
//  DevStart.h
//  DevStart
//
//  Created by Krystian Kulawiak on 05/12/2018.
//  Copyright © 2018 Krystian Kulawiak. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DevStart.
FOUNDATION_EXPORT double DevStartVersionNumber;

//! Project version string for DevStart.
FOUNDATION_EXPORT const unsigned char DevStartVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DevStart/PublicHeader.h>


